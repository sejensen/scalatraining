package model

import play.api.Logger

import java.sql.SQLException
import java.sql.Timestamp
import org.squeryl._
import org.squeryl.PrimitiveTypeMode._
import org.squeryl.dsl._
import ast.TypedExpressionNode

object Genre extends Enumeration {
  type Genre = Value
  val Comedy = Value(1, "Comedy")
  val Action = Value(2, "Action")
  val Animated = Value(3, "Animated")
  val Thriller = Value(4, "Thriller")
  val Drama = Value(5, "Drama")
}

import Genre._

case class Actor(val id:Int, val name:String, age:Int) extends KeyedEntity[Int]  {
  lazy val movies = MovieDb.starredInMovies.right(this) 
}

case class Movie(val id:Int, var title: String, var director: String, var imdb: Double,val genre:Genre) extends KeyedEntity[Int]  {
  def this() = this(0,"", "", 0.0,Genre.Comedy)
  override def toString = id+ ":" + title
  lazy val actors = inTransaction {MovieDb.starredInMovies.left(this)}
}

class StarredInMovie(var movieId: Int, var actorId: Int) extends KeyedEntity[CompositeKey2[Int,Int]] {
  override def toString = "StarredInMovie:" + id
  def id = compositeKey(movieId, actorId)
  
}

object Movie {
  import MovieDb._
  //In the next version of Squeryl there will be a singleOption instead of headOption. This should be smarter in cases where the programmer only expect one row as result
  def findById(movieId:Int) = inTransaction{movies.lookup(movieId)}
  
  def findByName(name:String):Option[Movie] = inTransaction {
      from(movies)(m => where(lower(m.title) like lower("%"+name.trim+"%"))
      select (m)).headOption
  }
  
  def findAll():List[Movie] = inTransaction {
      //Debugging statements, immdiately before query setLogger
      Session.currentSession.setLogger(Logger.info(_))
      movies.toList
  }
  
  //TODO insert, paging, etc
}

object MovieDb extends Schema {

  import org.squeryl.PrimitiveTypeMode._


  val movies = table[Movie]
  
  val actors = table[Actor]
  
  val starredInMovies = manyToManyRelation(movies, actors).via[StarredInMovie]((m,a,sim) => (sim.actorId === a.id, m.id === sim.movieId))

  on(movies) { m => declare {
    m.id is(autoIncremented)            
  }}

  on(actors) { a => declare {  
    a.id is(autoIncremented)            
  }}
  
  //TOTO indexer etc
  
}