import model._
import model.MovieDb._
import org.squeryl.adapters.H2Adapter
import play.api.{Logger, GlobalSettings, Application}

import org.squeryl.SessionFactory
import org.squeryl.Session
import play.api.db.DB
import org.squeryl.PrimitiveTypeMode._


object Global extends GlobalSettings {
  val dbAdapter = new H2Adapter
 
  override def onStart(app: Application) {
    SessionFactory.concreteFactory = Some(() =>
      Session.create(DB.getConnection()(app), dbAdapter
      ))
      
      transaction {
        MovieDb.drop
        MovieDb.create
        val morgan = actors.insert(Actor(0,"Morgan Freeman",52))
        val tim = actors.insert(Actor(0,"Tim Robbins",42))
        val shawshank = movies.insert(Movie(0,"Shawshank Redemption","Frank Darabont",9.2,Genre.Drama))
        starredInMovies.insert(new StarredInMovie(shawshank.id, morgan.id))
        starredInMovies.insert(new StarredInMovie(shawshank.id, tim.id))
        Logger.info("shawshank redemption starring: " + shawshank.actors.mkString(", "));
        
        //test debug
        Movie.findAll
      }
    }
}