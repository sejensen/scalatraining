package controllers

import play.api._
import play.api.mvc._
import model._
import model.MovieDb._

object Application extends Controller {
  
  def index = Action {
    Ok(views.html.index("Your new application is ready."))
  }
}

object Movies extends Controller {
  def index = Action{
    Ok(views.html.movies.index(Movie.findAll))
  }
  
  def show(id : Int) = Action {
    Ok(views.html.movies.show(Movie.findById(id).get))
  }
}