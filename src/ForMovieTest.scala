

class Actor(name:String,age:Int)

case class NormalActor(name:String,age:Int) extends Actor(name,age)
case class GreatActor(name:String, oscarSpeach:String,age:Int) extends Actor(name,age)

//import Genre._
case class Movie(name: String, starring:List[Actor],imdb:Double,genre:String)

/*object Genre extends Enumeration {
  type Genre = Value
  val Comedy = Value(1, "Comedy")
  val Action = Value(2, "Action")
  val Animated = Value(3, "Animated")
  val Thriller = Value(4, "Thriller")
  val Drama = Value(5, "Drama")
} */

object ForMovieTest extends App{
  val morgan = GreatActor("Morgan Freeman","Thank you, thank you god....",52)
  val tim = GreatActor("Tim Robbins","Thank you mom",42)
  val badActor = NormalActor("Steffen",33)
  val shawshank = Movie("Shawshank Redemption",List(morgan,tim),9.2,"Drama")
  val badMovie = Movie("Sommer i Tyrol",List(badActor),0.2,"Comedy")

  val movies = List(shawshank,badMovie)

  for {m <- movies
        a <- m.starring
        if (a match {
          case actor:GreatActor => true
          case _ => false
        })
  } yield a.oscarSpeach
}
