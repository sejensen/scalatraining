/*

 Scala syntax
 Cheat sheet :> http://docs.scala-lang.org/cheatsheets/
*/


//you can define a class with a val parameter, val in a class only define getter
class ClassWithVal(val name: String)

// you can define a class with var parameters, var in a class define getter and setter
class ClassWithVar(var description:String)

object Main extends App {
  println("Hello World: " + (args mkString ", "))

  println("Forget what you know about Java and C# and focus on Scala as a new language!\nThis will help you become better at Scala")

  /*classes, val and var */

  //a value is immutable and therefore cannot be changed
  val pi = 355.0/113

  var classWithVal = new ClassWithVal("No setter for name")
  //illegal
  //classWithVal.name = "new name"

  //classes and var
  var classWithVar = new ClassWithVar("First description")

  //vars can be reassigned
  classWithVar.description = "Changed description"

  println("Description for my classWithVar: "+classWithVar.description)


  /*Types and immutability */
  //types are inferred by the compiler (deduced) and can be omitted
  var mathResult = 323.0 + 234
  var tuple = ("Hello","World")
  var listOfStrings = List("Scala","Training", "at", "Systematic")

  //if I want to help the compiler or reader I can add types, but normally this is boilerplate (it is required in recursive calls)
  var result:Double = 232.0
  var strings:List[String] = List("Hello", "World")

  //In Scala we can omit the . to make the program more DSL like
  listOfStrings foreach println


  /*Functions*/

  // a function is defined as
  def addOne(i:Int) = i+1
  println(addOne(41))

  //anonymous functions can be saved as val to be passed around
  val myAFunc = (x:Int) => x+1

  //curried functions
  def multiply(m:Double)(n:Int):Double = m * n

  //call directly
  println(multiply(2)(3))

  //call later
  val timesPi = multiply(pi)(_)
  println(timesPi(2))


  /*Classes, objects and methods */
  class ClassWithMethod(var increment:Int = 0) {

    def myMethod(stringsToPrint:List[String]) = {
      increment +=1
      println(stringsToPrint.mkString(" "))
    }
  }

  val classWithMethod = new ClassWithMethod
  classWithMethod.myMethod(listOfStrings)
  println(classWithMethod.increment)


  class ClassWithDefaultConstructor(val name:String){
    def this() {
      this("defaultname")
    }
  }

  val classWithDefaultConstructor = new ClassWithDefaultConstructor()
  println(classWithDefaultConstructor.name)


  //if can return values
  val someValue = if (10>22){4} else {42}
  println(someValue)

  //if can return tuples
  //val tuple = if (10>22){(4,"Hest")} else {(42,"Hello")}
  val (some1,some2) = if (10>22){(4,"Hest")} else {(42,"Hello")}
  println(some2)

  //exceptions etc  try catch can return values
  val status = try {
    //remoteCalculatorService.add(1, 2)
    "OK"
  } catch {
    case e: IndexOutOfBoundsException => println("log ... the ....")
    case e1: NoSuchElementException => println("log ...the ...")
    case _ => println("log ...unkonwn exception")
    "Error"
  } finally {
    //remoteCalculatorService.close()
  }

  //for loops, ranges, etc
  for (i <- 0 until 10) {

  }

  for (i <- Range(0,10,2)) {
     println(i)
  }

  var greeting = ""
  args.foreach { arg =>
    greeting += (arg + " ")
  }


  val oddSquares = for(i <- 0 to 100 if i % 2 == 1) yield (i*i)
  oddSquares foreach println



}
