/*

Benifits of Case classes http://www.scala-lang.org/node/258

You can do pattern matching on it (more about that later),
You can construct instances of these classes without using the new keyword,
All constructor arguments are accessible from outside using automatically generated accessor functions,
The toString method is automatically redefined to print the name of the case class and all its arguments,
The equals method is automatically redefined to compare two instances of the same case class structurally rather than by identity.
The hashCode method is automatically redefined to use the hashCodes of constructor arguments.

Case classes are by default immutable
Case classes can have default arguments and has a convenient copy method
*/


object CaseClasses extends App {

  case class Person(name:String, email:String, age:Int)

  /*constructor equals, hashcode  */
  val p1 = Person("Steffen","sej@miracleas.dk",33)
  val p2 = Person("Hans","hah@test.com",33)
  //u can use new if you want, but why?
  val pNew = new Person("Test","test@test.com",32)

  println("equals "+(p1 == p1))
  println("equals "+(p1 eq p2))

  //convert hans to steffen
  val p3 = p2.copy(name = "Steffen",email = "sej@miracleas.dk")
  println("equals "+(p3 == p1))
  println("equals "+(p3.hashCode == p1.hashCode)+ " hashcode "+p1.hashCode)



  //access arguments
  println(p3.name)
  //illegal reassignment
  //p3.name = "Hans"

  //can have default arguments
  case class Employee(name:String, email:String, age:Int, var seniority:Int = 0, var role:String="Programmer",company:String = "Miracle")

  val hans = Employee("Hans","hah@test.com",28)
  val steffen = Employee(p1.name,p1.email,p1.age,5)
  val steffenNextYear = steffen.copy(seniority = 6)
  //Convenient toString
  println(steffen)
  println(steffen.hashCode + " = " + steffenNextYear.hashCode)

  //case classes can be converted to tuples
  println(Employee.unapply(steffen).get)


  //used in collections
  val employees1 = List(hans, steffen)
  //or
  val employees2 = List(Employee("Hans","hah@test.com",28), Employee(p1.name,p1.email,p1.age,5))

  //convert to string
  println(employees2 mkString ", ")

  //Inheritance... simple class model (remmeber not to extend case classes with case classes
  class Vehicle(model:String, allowedOnRoad:Boolean)
  case class Truck(model:String,maxLoad:Int, allowedOnRoad:Boolean= true) extends Vehicle(model,allowedOnRoad)
  case class RaceCar(model:String, allowedOnRoad:Boolean= true) extends Vehicle(model,allowedOnRoad)

  //testing
  val truck = Truck("Volvo",10000)
  println(truck)
  val raceCar = RaceCar("Farrari")
  println(raceCar)

  /*raceCar match {
    case t:Truck => println(t)
    case _ => println("other")
  }
         */


}
