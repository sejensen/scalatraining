/*

LAB 2:

 1.Create a Scala main app with a movie model with movies, starring (collection), director, genre, language and imdb rating (hint use http://www.imdb.com/chart/top as data inspiration)
 2.Use Traits to define common behavior of actors, director and writer (executeStunt, oscarSpeech, impersonate???)
 3.Populate a collection of your favorite movies and define actors and writers (use http://www.imdb.com/chart/top as data inspiration)
 4.Find all movies with Comedy as Genre using pattern matching and Put the logic in a Object Helper
 5.Find all movies where actress “name” stars
 6.Sort your movies based on IMDB rating
 7.Order your movies by language
*/

//model


object Lab2 extends App {
  // code here


}
