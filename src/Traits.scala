object Traits extends App {

  case class Person(name: String)

  //mixed in at object creation
  val me = Person("Steffen")

  //compile error
  //me.fight

  case class Car(name:String)

  trait Ninja{
    def name:String
    def kick = println(name + "Kicking")
    def jump = println("Jumping")
    def fight = {
      kick
      jump
      kick
    }
    override def toString = "my ninja toString"
  }

  val meAsNinja = new Person("Steffen") with Ninja

  meAsNinja.fight
  println(meAsNinja.toString)

  val myCar = new Car("Farrari") with Ninja
  myCar.fight
  println(myCar.getClass)



}
