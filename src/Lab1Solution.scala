//one possible solution to lab 1 wihtout inheritance part
object Lab1Solution extends App {
  //1
  case class Foo(val c: Int = 1)

  //2
  val a = List(Foo(1), Foo(2), Foo(100), Foo(35423))

  //3
  val max = a.map(_.c).max

  //4
  val (u, o) =  a.partition(_.c < max/2)

  //6
  val sum1 = a.map(_.c * 42).sum
  val sum2 = a.foldLeft(0)((sum, e) => sum + 42 * e.c)

  // 7
  val fancyFormatting = a.mkString("[", "," ,"]")
}