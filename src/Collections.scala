/*


http://www.scala-lang.org/docu/files/collections-api/collections.html


 */

object Collections extends App {

  //definition
  val list = List(1,2,3,4,5,6,7,8,9)

  val set = Set(1,2,3,4,5,6)

  val map = Map(1 ->2, 2->4)

  val tuple = ("localhost",8080)

  //special way to get values
  println("tuple data "+tuple._1 +" "+tuple._2)

  //simple helper to make examples cleaner
  class PrintableCollection(c:Traversable[_]){
    def print = println(c.mkString(","))
  }
  implicit def collectionToPrintableCollection(c:Traversable[_]) = new PrintableCollection(c)

  //functional combinators

  //map evaluates a function over each element in a collection and returns a collection with same size and same type as "input" collection
  list.map((i:Int) => i*2).print

  list.map {
    _ * 16
  }.print

  //foreach is like map but returns nothing (intended for side effects
  set.foreach((i: Int) => i * 2)

  //filter removes any elements where the function you pass in evaluates to false
  list.filter((i:Int)=> i%2 == 0).print

  //zip aggregates the contents of two lists into a single list of pairs.
  list.zip(set.toList).print

  //returns the first element that matches
  val result = list.find((i: Int) => i > 5)
  println("found "+result)

  //sorted sorts the
  List(1,54,23,3,42,102,2,335,5).sorted.print

  list.drop(4).print


  //accessing data
  case class Person(name:String, email:String, age:Int)

  val people = List(Person("Steffen Jensen","sej@miracleas.dk",33),Person("Claus Christiansen","claus@mail.com",25),Person("Michael Christiansen","michael@mail.com",20))

  val me = people.head
  val perhapsMe = people.headOption
  //accessing values
  val accessed = List(people(2),people.head,people.tail)
  accessed.print


  val peopleMap = Map("steffen" -> people(0), "claus" -> people(1), "michael" -> people(2))
  println(peopleMap.get("steffen"))
  println(peopleMap.getOrElse("notHere","None"))

  peopleMap.filter((p:(String,Person))=>p._2.age <= 20).print
  peopleMap.filter((p:(String,Person))=>p._2.age > 20).print

  peopleMap.keySet.print
  peopleMap.values.map {_.name}.print

  //working with java collections
  import scala.collection.JavaConversions._
  val sl = new scala.collection.mutable.ListBuffer[Int]
  val jl : java.util.List[Int] = sl
  val sl2 : scala.collection.mutable.Buffer[Int] = jl
  println("java collection wrapped equals to scala collection " +(sl eq sl2))


  //for comprehensions http://www.scala-lang.org/node/47
  def olderThan20(xs: List[Person]): List[String] = {
    // The first expression is called a 'generator' and makes
    // 'p' take values from 'xs'. The second expression is
    // called a 'filter' and it is a boolean expression which
    // selects only persons older than 20. There can be more than
    // one generator and filter. The 'yield' expression is evaluated
    // for each 'p' which satisfies the filters and used to assemble
    // the resulting list
    for (p <- xs if p.age > 20) yield p.name
  }

  olderThan20(people).print

  val first = List(1, 2)
  val next = List(8, 9)
  val last = List("ab", "cde", "fghi")

  for {
    i <- first
    j <- next
    k <- last
  }
  yield(i * j * k.length)


  /** Return the divisors of n. */
  def divisors(n: Int): List[Int] =
    for (i <- List.range(1, n+1) if n % i == 0) yield i

  /** Is 'n' a prime number? */
  def isPrime(n: Int) = divisors(n).length == 2


  val n = 100
  val primes = for {
    i <- 1 until n
    j <- 1 until (i-1)
    if isPrime(i+j)
  }
  yield (i, j,i+j)

  primes.print

  primes.map(_._3).print


}
