/*

Why pattern matching
explained by Martin Odersky here http://www.artima.com/scalazine/articles/pattern_matching.html

*/

object PatternMatching extends App {
  //simple helper to make examples cleaner
  class PrintableCollection(c:Traversable[_]){
    def print = println(c.mkString(", "))
  }
  implicit def collectionToPrintableCollection(c:Traversable[_]) = new PrintableCollection(c)


  val data = List("hej", 43, "med", 49, "dig")
  val filteredStrings = data.map(_ match {
    case s: String => s
    case _ => ""
  })


  val capitals = Map("denmark"->"copenhagen","norway"->"oslo","sweeden"->"stokholm")
  capitals.print

  println(capitals.get("norway")) //some
  println(capitals.get("madagascar")) //unknown

  println(capitals.getOrElse("madagascar","unknown"))



  val times = 1
  //matching on values
  times match {
    case 1 => "one"
    case 2 => "two"
    case _ => "some other number"
  }

  //mathing with guards
  times match {
    case i if i == 1 => "one"
    case i if i == 2 => "two"
    case _ => "some other number"
  }

  //mathing authority example from play20-auth module for play 2
  class Permission(role:String) {
    override def toString = role
  }
  case object Administrator extends Permission("Administrator")
  case object NormalUser extends Permission("NormalUser")
  type Authority = Permission

  case class User(id: Long = 0, username: String, password: String,permission:String)

  def authorize(user: User, authority: Authority) = {
    val authorized = (user.permission, authority) match {
      case ("Administrator", _) => true
      case ("NormalUser",NormalUser) => true
      case _ => false
    }
    //Logger.info("authorizing user: "+user.username + ", with permission: "+user.permission)
    authorized
  }

  //real world example from scala backend
  //dummy model
  class Plan
  class ManualPlan extends Plan
  class FrequencyPlan extends Plan
  class BidPlan extends Plan

  //using pattern matching on classes
  private def invalidateExistingPlans(plan: Plan) = {
    //require(plan != null && plan.day != null && plan.plant != null, "plan - " + plan + " - " + plan.plant)
    plan match {
      case mp: ManualPlan => //..
      case fp: FrequencyPlan => //..
      case bp: BidPlan => Nil
      case _ => //..
    }
    //...
  }

  //using pattern mathing on string compares to switch case in java
  def initiateSendPlanFlow(day: String, planType: String) {
    //...
    try {
      planType match {
        case "BidPlan" =>
        //...
        case "ProductionPlan" =>
        //...
        case "ManualPlan" =>
        //...
        case "FrequencyPlan" =>
        //...
        case "EffectPlan" =>
        //...
        case "RegBid" =>

        case _ => throw new Exception("anmodet om ukendt type " + planType)
      }
    }
  }


  //case class Person(name: String, email: String, age: Int)


}
