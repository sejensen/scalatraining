/*

LAB 1:

1. Create a model based on inheritance of your favorite items, books, languages, cars, fruit. Model should at least contain a name and count attribute
2. Create a collection of objects of this model and assign it to a val
3. Use collection api to iterate through all models and find max count in list   (HINT can be solved by using _ max and map)
4. Use collection api to split models in two collections with count less than max/2 in one and the other >= max/2
5. Use collection api to apply any function on all objects
6. Create a function that multiplies “count” with a number and calculate the sum
7. Use mkString to compose a comma separated list of names
8. Try out the different collection api methods contains, foreach, filter, zip, partition, find, foldLeft, flatten, flatMap  -> look here for help http://twitter.github.com/scala_school/collections.html or here for detailed instructions http://www.scala-lang.org/api/current/index.html#ollections-api.collections

*/

//model


object Lab1 extends App {

}
