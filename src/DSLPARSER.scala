import scala.util.parsing.combinator.syntactical._
import scala.util.parsing.combinator._

//to be able to compile
case class InstrumentReport(measurements:List[Measurement])
case class Measurement(id:String, pid:String,bid:String,name:String,mType:String, var fp:String="", var resText:String = "")


object EParser extends RegexParsers {

  def SEP  = "----------[0-9]+[\n]".r
  def TXT  = "[^\",\r\n]+".r
  def CRLF = "\r\n" | "\n"
  def REST = "[.\r\n]*".r
  def BLOCK_START = "^\\$[a-zA-Z]+%".r
  def BLOCK_SEP = "------"
  def VERSION = "Version: [0-9]+[\n]".r

  def file: Parser[InstrumentReport] = (VERSION) ~> instrument_data~structured_items ^^ {case instrument_data~structured_items  => InstrumentReport(structured_items)}      //~header_opr~header_inst~end_table_data~report_comments

  def instrument_data: Parser[String] =  (BLOCK_START)~> (TXT) <~ BLOCK_SEP ^^{ case _ => ""}

  def structured_items:Parser[List[Measurement]] = (BLOCK_START)~> repsep(measurement, SEP) <~ (SEP?)

  def measurement: Parser[Measurement] = base~opt(extension) ^^ {case base~extension => {
    //base.fp = "SUPERMAND ER SEJ"
    if (base.mType == "23"){
      extension match {
        case Some(s) => {
          val extTxt = s.split(",")
          base.fp = extTxt.last
          base.resText = extTxt.first}
        case _ => ""
      }
    }
    base
  }}//repsep(id~pid, CRLF)

  def base: Parser[Measurement] = id~pid~bid~name~mtype ^^ {case id~pid~bid~name~mtype => Measurement(id,pid,bid,name,mtype)} /*new Measurement(id, pid, bid, name, mtype) */

  def id: Parser[String] = "ID: "~> (TXT) ^^ { case s => s.mkString("") }
  def pid: Parser[String] = "PID: "~> (TXT) ^^ { case s => s.mkString("") }
  def bid: Parser[String] = "BID: "~> (TXT) ^^ { case s => s.mkString("") }
  def name: Parser[String] = "Name: "~> (TXT) ^^ { case s => s.mkString("") }
  def mtype: Parser[String] = "Type: "~> (TXT) ^^ { case s => s.mkString("") }

  def extension: Parser[String] =(o_edits | b_edits~b_checks | rep(res_text))~opt(fp_string) ^^  {
    case (ts: List[String])~Some(fp_string) => ts.mkString(",")+","+fp_string//ts.mkString
    case (s:String)~Some(fp_string) => ""+fp_string//s.mkString("")
    case b_edits~b_checks~fp_string => ""+b_edits+b_checks //
    case _ =>""
  }

  def o_edits: Parser[String] = "O_Edits: "~> (TXT) ^^ { case s => s.mkString("") }
  def b_edits: Parser[String] = "B_Edits: "~> (TXT) ^^ { case s => s.mkString("") }
  def b_checks: Parser[String] = "B_Checks: "~> (TXT) ^^ { case s => s.mkString("") }
  def res_text: Parser[String] = "ResText: "~> (TXT) ^^ { case s => s.mkString("") }
  def fp_string: Parser[String] = "FP_String: "~> (TXT) ^^ { case s => s.mkString("") }

  def field: Parser[String] = (TXT*) ^^ { case ls => ls.mkString("") }

  def parse(s: String):InstrumentReport = parseAll(file, s) match {
    case Success(res, _) => res
    case e => throw new Exception(e.toString)
  }
}