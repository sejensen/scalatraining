package controllers

import play.api._
import play.api.mvc._
import models._

object Application extends Controller {
  
  def index = Action {
    Ok(views.html.index("Your new application is ready."))
  }
  
}

object Movies extends Controller {
  def index = Action{
    Ok(views.html.movies.index(MovieStore.list))
  }
  
  def show(id : Int) = Action {
    Ok(views.html.movies.show(MovieStore.get(id).get))
  }
}