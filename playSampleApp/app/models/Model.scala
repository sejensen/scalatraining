package models

import play.api.Play.current


case class Actor(name:String)

case class Movie(id:Int,name: String, starring:List[Actor])

object MovieStore {
  import scala.collection._
  val store = mutable.HashMap[Int, Movie]()
  store += 0 -> Movie(0,"ShawShank Redemption",List())
  store += 1 -> Movie(1,"The Matrix",List())
  store += 2 -> Movie(2,"Shrek",List())
  
  def list = store.values.toList
  
  def get(id:Int):Option[Movie] = store.get(id)
  
  def put(movie:Movie) = store += (store.keySet.max+1) -> movie
}